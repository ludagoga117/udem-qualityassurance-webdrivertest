#language: es

Característica: Procesos Judiciales por nombre
    David como ciudadano
    Quiere consultar sus procesos judiciales por nombre
    Para ver los avances de todos sus procesos judiciales

    Escenario: Consultar los procesos judiciales de David
      Dado que "David" esta consultando el proceso judicial
      Cuando selecciona Ciudad "MEDELLIN", entidad "JUZGADOS PENALES DEL CIRCUITO DE MEDELLIN" y opcion de consulta "Consulta por Nombre o Razón social"
      Entonces debe poder ver el sujeto procesal