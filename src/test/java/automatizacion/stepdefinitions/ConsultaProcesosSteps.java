package automatizacion.stepdefinitions;

import automatizacion.page.RamaJudicialInicioPage;
import automatizacion.question.TextoBarra;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static automatizacion.page.ConsultaProcesosPage.*;
import static automatizacion.page.RamaJudicialInicioPage.BTN_CONSULTAR_PROCESOS;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.core.StringContains.containsString;

public class ConsultaProcesosSteps {

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("que \"(.*)\" esta consultando el proceso judicial")
    public void queEstaConsultandoElProcesoJudicial(String actor) {
        theActorCalled(actor).attemptsTo(
                Open.browserOn().the(RamaJudicialInicioPage.class),
                Scroll.to(BTN_CONSULTAR_PROCESOS).andAlignToBottom(),
                Click.on(BTN_CONSULTAR_PROCESOS)
        );
    }

    @Cuando("selecciona Ciudad \"(.*)\", entidad \"(.*)\" y numero de radicado \"(.*)\"")
    public void ingresarCiudadEntidadYRadicado(String ciudad, String entidad, String radicado) {
        theActorInTheSpotlight().attemptsTo(
                SelectFromOptions.byVisibleText(ciudad).from(SEL_CIUDAD),
                SelectFromOptions.byVisibleText(entidad).from(SEL_ENTIDAD),
                Enter.theValue(radicado).into(INPUT_RADICADO)
        );
    }

    @Entonces("debe poder activar la consulta")
    public void verificarActivacionConsulta() {
        //BrowseTheWeb.as(theActorInTheSpotlight()).getDriver().findElement()

        theActorInTheSpotlight().should(
                seeThat(
                        "the displayed username",
                        TextoBarra.barra(),
                        containsString("Deslice la barra a la derecha")
                        )
        );
    }
}