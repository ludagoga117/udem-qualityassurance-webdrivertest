package automatizacion.stepdefinitions;

import automatizacion.question.TextoTipoSujeto;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static automatizacion.page.ConsultaProcesosPage.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.core.StringContains.containsString;

public class ConsultaProcesosPorNombreSteps {
    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Cuando("selecciona Ciudad \"(.*)\", entidad \"(.*)\" y opcion de consulta \"(.*)\"")
    public void seleccionaCiudadEntidadYOpcionDeConsulta(String ciudad, String entidad, String opcionDeConsulta) {
        theActorInTheSpotlight().attemptsTo(
                SelectFromOptions.byVisibleText(ciudad).from(SEL_CIUDAD),
                SelectFromOptions.byVisibleText(entidad).from(SEL_ENTIDAD),
                SelectFromOptions.byVisibleText(opcionDeConsulta).from(SEL_OPCION_DE_CONSULTA)
        );
    }

    @Entonces("debe poder ver el sujeto procesal")
    public void debePoderVerElSujetoProcesal() {
        theActorInTheSpotlight().should(
                seeThat(
                        "Etiqueta tipo sujeto",
                        TextoTipoSujeto.tipoSujeto(),
                        containsString("* Tipo Sujeto:")
                )
        );
    }
}
