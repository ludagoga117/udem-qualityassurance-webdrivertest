package automatizacion.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
// Screenplay
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/consultarprocesospornombre.feature",
        glue = {"automatizacion.stepdefinitions"},
        snippets = SnippetType.CAMELCASE
)
public class ConsultarProcesosPorNombreRunner {

}