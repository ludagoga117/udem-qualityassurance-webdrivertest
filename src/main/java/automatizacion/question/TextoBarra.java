package automatizacion.question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static automatizacion.page.ConsultaProcesosPage.TEXT_BARRA;

public class TextoBarra implements Question<String> {

    @Override
    public String answeredBy(Actor actor) {
        return TEXT_BARRA.resolveFor(actor).getText();
    }

    public static TextoBarra barra() {
        return new TextoBarra();
    }
}