package automatizacion.question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static automatizacion.page.ConsultaProcesosPage.TEXT_TIPO_SUJETO;

public class TextoTipoSujeto implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return TEXT_TIPO_SUJETO.resolveFor(actor).getText();
    }

    public static TextoTipoSujeto tipoSujeto() {
        return new TextoTipoSujeto();
    }
}
